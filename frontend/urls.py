from django.urls import path
from . import views

urlpatterns = [
    path("home/", views.home, name="home"),
    path("create/", views.create_room, name="create"),
    path("join/", views.join_room, name="join"),
    path("room/<str:code>/", views.room, name="room"),
    path("settings/<int:votes>/<str:guest>/<str:code>", views.settings),
]