from django.shortcuts import render

# Create your views here.


def home(request):

    return render(request, "frontend/home.html", {})


def create_room(request):

    return render(request, "frontend/create_room.html", {})


def join_room(request):

    return render(request, "frontend/room_join.html", {})


def room(request, code):

    return render(request, "frontend/room.html", {"code": code})


def settings(request, votes, guest, code):
    print(votes)
    context = {
        "guest_can_pause": guest,
        "votes_to_skip": votes,
        "code": code,
    }
    return render(request, "frontend/settings.html", context=context)