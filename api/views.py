from django.shortcuts import render, HttpResponse
from rest_framework import generics, status
from .models import Room
from .serializers import RoomSerialzier, CreateRoomSerializer, UpdateRoomSerializer

from rest_framework.views import APIView  # generic API
from rest_framework.response import Response

from django.http import JsonResponse

# here I will write the endpoints


class RoomView(generics.CreateAPIView):
    # it will allow us view and create rooms

    queryset = Room.objects.all()
    serializer_class = RoomSerialzier


class RoomDetails(APIView):
    """
    this view is to retrieve the room details
    """

    serializer_class = RoomSerialzier
    lookup_url_kwargs = "code"  # I need to pass a parameter in the url called 'code'

    def get(self, request, code, format=None):
        code = code

        print(code)

        if code != None:
            room = Room.objects.filter(code=code)
            if len(room) > 0:
                data = RoomSerialzier(room[0]).data
                data["is_host"] = self.request.session.session_key == room[0].host
                return Response(data, status=status.HTTP_200_OK)
            return Response(
                {"Bad Request": "Invalid Room"}, status=status.HTTP_404_NOT_FOUND
            )

        return Response(
            {"Bad Request": "code not found"}, status=status.HTTP_400_BAD_REQUEST
        )


class JoinRoom(APIView):
    """
    this is a view to join a room
    here I am using POST request
    """

    def post(self, request, format=None):
        # here I want to check if the user has active session or not

        if not self.request.session.exists(self.request.session.session_key):
            self.request.session.create()

        # get the code form the POST request
        code = request.data.get("code")

        if code != None:
            room_res = Room.objects.filter(code=code)
            if len(room_res) > 0:
                room = room_res[0]
                self.request.session["room_code"] = code
                return Response({"message": "Joined!!"}, status.HTTP_200_OK)
            return Response(
                {"Bad Request": "Not Valid Code"}, status.HTTP_400_BAD_REQUEST
            )

        return Response(
            {"Bad Resquest": "Invalid code"}, status=status.HTTP_400_BAD_REQUEST
        )


class CreatRoomView(APIView):
    # using the APIView I can override some
    # pre written methods

    serializer_class = CreateRoomSerializer

    def post(self, request, format=None):
        # check if the current user has an active session within the server
        if not self.request.session.exists(self.request.session.session_key):
            self.request.session.create()  # if not create new session
        else:
            pass

        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            guest_can_pause = serializer.data.get("guest_can_pause")
            votes_to_skip = serializer.data.get("votes_to_skip")
            host = self.request.session.session_key  # add the session key

            # now I need to check if the user who is trying to create new room
            # already has one, so I will update its settings
            queryset = Room.objects.filter(host=host)
            if queryset.exists():
                # grap the active room
                room = queryset[0]
                room.guest_can_pause = guest_can_pause
                room.votes_to_skip = votes_to_skip
                # save it
                room.save(
                    update_fields=["guest_can_pause", "votes_to_skip"]
                )  # the fields I want to update
                self.request.session["room_code"] = room.code
                return Response(RoomSerialzier(room).data, status=status.HTTP_200_OK)
            else:
                # if not: create new room
                room = Room(
                    host=host,
                    guest_can_pause=guest_can_pause,
                    votes_to_skip=votes_to_skip,
                )
                room.save()
                self.request.session["room_code"] = room.code
                return Response(RoomSerialzier(room).data, status=status.HTTP_200_OK)

        # now I want to send a response to tell if it was valid or not
        return Response(
            RoomSerialzier(room).data, status=status.HTTP_201_CREATED
        )  # return the data


class UserInRoom(APIView):
    def get(self, request, format=None):
        if not self.request.session.exists(self.request.session.session_key):
            self.request.session.create()
        data = {"code": self.request.session.get("room_code")}

        return JsonResponse(data, status=status.HTTP_200_OK)


class LeaveRoom(APIView):
    def post(self, request, format=None):
        if "room_code" in self.request.session:
            self.request.session.pop("room_code")
            host_id = self.request.session.session_key

            room_res = Room.objects.filter(host=host_id)
            if len(room_res) > 0:
                room = room_res[0]
                room.delete()  # delete the room.

        return Response({"message": "success"}, status=status.HTTP_200_OK)


class UpdateRoom(APIView):

    serializer_class = UpdateRoomSerializer

    def patch(self, request, format=None):
        if not self.request.session.exists(self.request.session.session_key):
            self.request.session.create()  # if not create new session
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            # get the data
            guest_can_pause = serializer.data.get("guest_can_pause")
            votes_to_skip = serializer.data.get("votes_to_skip")
            code = serializer.data.get("code")

            queryset = Room.objects.filter(code=code)
            if not queryset.exists():
                return Response(
                    {"message": "Room not found"}, status=status.HTTP_404_NOT_FOUND
                )

            room = queryset[0]

            # Now I need to make sure that the user is the owner
            user_id = self.request.session.session_key
            if room.host != user_id:
                return Response(
                    {"message": "Not Authorized"}, status=status.HTTP_404_NOT_FOUND
                )
            room.guest_can_pause = guest_can_pause
            room.votes_to_skip = votes_to_skip
            room.save(update_fields=["guest_can_pause", "votes_to_skip"])

            return Response(RoomSerialzier(room).data, status=status.HTTP_200_OK)

        return Response(
            {"Bad Request": "Invalid Data"}, status=status.HTTP_400_BAD_REQUEST
        )
