from django.urls import path
from . import views


urlpatterns = [
    path("room", views.RoomView.as_view(), name="home"),
    path("create/", views.CreatRoomView.as_view()),
    path("details/<str:code>/", views.RoomDetails.as_view(), name="detail"),
    path("join/", views.JoinRoom.as_view(), name="join"),
    path("user-in-room/", views.UserInRoom.as_view()),
    path("leave/", views.LeaveRoom.as_view()),
    path("update/", views.UpdateRoom.as_view()),
]
